import { TemplateController } from 'meteor/template-controller';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import SimpleSchema from 'simpl-schema';
import Rickshaw from 'rickshaw';
import './rickshaw.html';

checkNpmVersions({
  'simpl-schema': '1.5.3',
  // todo используется версия зависимости
  //d3: '5.6.0',
}, 'rickshaw');

const SerieSchema = new SimpleSchema({
  color: { type: String, optional: true },
  data: { type: Array },
  'data.$': { type: Object },
  'data.$.x': { type: Number },
  'data.$.y': { type: Number },
});

TemplateController('Rickshaw', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    // тип графика
    type: { type: String, allowedValues: ['line', 'bar', 'area', 'stack', 'scatterplot'] },
    series: { type: Array },
    'series.$': { type: SerieSchema },
    stack: { type: Boolean, defaultValue: true },
    max: { type: Number, optional: true },
    min: { type: Number, optional: true },
    // опции для графика, проверяется самим rickshaw
    options: { type: Object, blackbox: true, defaultValue: {} },
    // функция для возврата объекта графика
    handleObject: { type: Function, optional: true },
    className: { type: String, defaultValue: '' },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    const {
      type, max, stack, handleObject, options,
    } = this.props;
    this.graph = new Rickshaw.Graph({
      element: this.find('div'),
      renderer: type,
      // мутирует данные
      series: [{ data: [] }],
      max,
      stack,
      ...options,
    });
    // Если нужно вернуть график для работы с ним
    if (handleObject) {
      handleObject(this.graph);
    }
    this.autorun(() => {
      // клонирование данных, т.к. graph мутирует данные
      const series = this.props.series.map(s => Object.assign({}, s));
      this.graph.setSeries([...series]);
      this.graph.render();
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    graph: null,
  },
});
