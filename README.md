# Rickshaw

Blaze-Компонент реализующий работу графиков [rickshaw.js](http://shutterstock.github.io/rickshaw/).
Работает на основе d3.

### Вызов компонета
```spacebars
{{> Rickshaw chart}}
```
### Параметры 
```javascript
const props = {
  // тип графика
  type: { type: String, allowedValues: ['line', 'bar', 'area', 'stack', 'scatterplot'] },
  // набор данных
  series: { type: Array },
  'series.$': { type: SerieSchema },
  // опции для графика
  stack: { type: Boolean, defaultValue: true },
  max: { type: Number, optional: true },
  min: { type: Number, optional: true },
  // остальные опции для графика, проверяется самим rickshaw
  options: { type: Object, blackbox: true, defaultValue: {} },
  // функция для возврата объекта графика
  handleObject: { type: Function, optional: true },
  // класс для обертки графика
  className: { type: String, defaultValue: '' },
}
```
#### Схема данных для графика
```javascript
const SerieSchema = new SimpleSchema({
// цвет
  color: { type: String, optional: true },
  // набор данных
  data: { type: Array },
  'data.$': { type: Object },
  'data.$.x': { type: Number },
  'data.$.y': { type: Number },
});
```

