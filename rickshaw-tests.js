// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by rickshaw.js.
import { name as packageName } from "meteor/rickshaw";

// Write your tests here!
// Here is an example.
Tinytest.add('rickshaw - example', function (test) {
  test.equal(packageName, "rickshaw");
});
