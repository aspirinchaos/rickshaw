Package.describe({
  name: 'rickshaw',
  version: '0.1.1',
  summary: 'Blaze component for Rickshaw',
  git: 'https://bitbucket.org/aspirinchaos/rickshaw.git',
  documentation: 'README.md',
});

Npm.depends({
  rickshaw: '1.6.6',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'fourseven:scss',
    'templating',
    'template-controller',
  ]);
  api.mainModule('rickshaw.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('rickshaw');
  api.mainModule('rickshaw-tests.js');
});
